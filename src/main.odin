package main;

import "core:fmt"
import "core:encoding/json"
import "core:os"
import "core:strings"
import "odbc"

AppFlags :: enum
{
	HELP,
}

AppFlagSet :: bit_set[AppFlags]

FileAppArgs :: struct
{
	token: string,
	odbcConnectionString: string,
}

AppArgs :: struct
{
	program: string,
	using args: FileAppArgs,
	flags: AppFlagSet,
}

AppArgState :: enum u8
{
	UNKNOWN,
	TOKEN,
	CONNECTION_STRING,
}

AppError :: enum u8
{
	NO_TOKEN,
	INVALID_TOKEN,
	NO_CONNECTION_STRING,
}

AppErrors :: bit_set[AppError; int]

FileArgState :: enum u8
{
	UNKNOWN,
	CONFIG,
}

FileArgs :: struct
{
	configPath: string,
}

ParseArgsJsonError :: union
{
	json.Unmarshal_Error,
	bool,
}

main :: proc()
{
	start(os.args);
}

start :: proc(args: []string)
{
	appArgs: AppArgs;
	fileArgs: FileArgs = {
		configPath = "appsettings.json",
	};
	err: AppErrors;
	parseArgs(&fileArgs, args);
	jsonErr: ParseArgsJsonError = parseJson(
		&(appArgs.args),
		fileArgs.configPath
	);
	if jsonErr != nil
	{
		fmt.printfln("%v", jsonErr);
	}
	parseEnv(&appArgs);
	parseArgs(&appArgs, args);
	err = validateArgs(&appArgs);
	usage(appArgs, err);
	for
	{
		fmt.printf("%s\r", appArgs.token);
		break;
	}
}

parseArgs :: proc{
	parseArgs1,
	parseArgs2,
}

parseArgs1 :: proc(arguments: ^FileArgs, args: []string) {
	state: FileArgState = .UNKNOWN;
	for &arg, i in args
	{
		switch state
		{
		case .UNKNOWN:
			switch arg
			{
			case "--config":
				state = .CONFIG;
			}
		case .CONFIG:
			arguments.configPath = arg;
		}
	}
}

parseArgs2 :: proc(arguments: ^AppArgs, args: []string)
{
	state: AppArgState = .UNKNOWN;
	loop: for &arg, i in args
	{
		switch state
		{
		case .UNKNOWN:
			switch arg
			{
			case "--token":
				state = .TOKEN;
			case "--odbc":
				state = .CONNECTION_STRING;
			case:
				switch i
				{
				case 0:
					arguments.program = arg;
				case 1:
					if (arg == "help")
					{
						arguments.flags |= { .HELP };
						break loop;
					}
				}
				state = .UNKNOWN;
			}
		case .TOKEN:
			arguments.token = arg;
			state = .UNKNOWN;
		case .CONNECTION_STRING:
			arguments.odbcConnectionString = arg;
			state = .UNKNOWN;
		}
	}
}

parseEnv :: proc(args: ^AppArgs)
{
	using args;
	envLookup("DESPAM_TOKEN", &token);
	envLookup("DESPAM_ODBC", &odbcConnectionString);
}

envLookup :: proc
{
	envLookup_string,
}

envLookup_string :: proc($envVar: string, arg: ^string)
{
	arg^ = os.lookup_env(envVar) or_else arg^;
}

parseJson :: proc(
	args: ^FileAppArgs,
	filePath: string
) -> ParseArgsJsonError
{
	contents := os.read_entire_file_from_filename(filePath) or_return;
	defer delete(contents);
	json.unmarshal(contents, args) or_return
	return nil;
}

help :: proc(program: string)
{
	fmt.printf(
`Usage: %s [OPTIONS]
Configurations are applied in the order shown.
CONFIGURATION OPTIONS:
    --config <path>: Path to json configuraiton file. Defaults to
        appsettings.json
JSON FILE:
    "token": The full authorizaiton token header value
        (including the token type).
	"odbcConnectionString": An ODBC formatted connection string.
ENVIRONMENT:
    DESPAM_TOKEN: The full authorizaiton token header value
        (including the token type).
    DESPAM_ODBC: An ODBC formatted connection string.
OPTIONS:
    --token <authroizaiton token>: The full authorizaiton token header value
        (including the token type).
    --connection-string <connection string>: An ODBC formatted connection
		string.
`, program
	);
}

usage :: proc(args: AppArgs, err: AppErrors)
{
	using args;
	if .NO_TOKEN in err
	{
		fmt.println("No token was provided.");
	}
	if .INVALID_TOKEN in err
	{
		fmt.println("Invalid token was provided.");
		fmt.println(
			"    Valid tokens are formatted <Bearer|Bot> <token contents>"
		);
	}
	if .NO_CONNECTION_STRING in err
	{
		fmt.println("No connection string was provided.");
	}
	if (err != {}) | (.HELP in flags)
	{
		help(program);
		os.exit(transmute(int)err);
	}
}

validateArgs :: proc(args: ^AppArgs) -> AppErrors
{
	using args;
	using strings;
	ret: AppErrors;
	if .HELP not_in flags
	{
		tokenSplit2, _ := split_n(token, " ", 2);
		tokenSplit3, _ := split_n(token, " ", 3);
		odbcSplit, _ := split(odbcConnectionString, ";");
		defer delete(tokenSplit2);
		defer delete(tokenSplit3);
		defer delete(odbcSplit);
		switch
		{
		case token == "":
			ret |= { .NO_TOKEN };
		case len(tokenSplit2) != 2:
		case len(tokenSplit3) != 2:
			ret |= { .INVALID_TOKEN };
		}
		switch
		{
			case odbcConnectionString == "":
				ret |= { .NO_CONNECTION_STRING };
		}
	}
	return ret;
}
