package odbc;

import "core:mem"
import "core:strings"

foreign import sql "system:odbc";
@(private="file")
foreign sql
{
	SQLAllocConnect :: proc(
		EnvironmentHandle: SQLHENV,
		ConnectionHandle: [^]SQLHDBC
	) -> SQLRETURN ---
	SQLAllocEnv :: proc(EnvironmentHandle: [^]SQLHENV) -> SQLRETURN ---
	SQLAllocStmt :: proc(
		ConnectionHandle: SQLHDBC,
		StatementHandle: [^]SQLHSTMT
	) -> SQLRETURN ---
	SQLBindCol :: proc(
		StatementHandle: SQLHSTMT,
		ColumnNumber: SQLUSMALLINT,
		TargetType: SQLSMALLINT,
		TargetValue: SQLPOINTER,
		BufferLength: SQLLEN,
		StrLen_or_Ind: [^]SQLLEN
	) -> SQLRETURN ---
	SQLBindParam :: proc(
		StatementHandle: SQLHSTMT,
		ParameterNumber: SQLUSMALLINT,
		ValueType: SQLSMALLINT,
		ParameterType: SQLSMALLINT,
		LengthPrecision: SQLULEN,
		ParameterScale: SQLSMALLINT,
		ParameterValue: SQLPOINTER,
		StrLen_or_Ind: [^]SQLLEN
	) -> SQLRETURN ---
	SQLCancel :: proc(StatementHandle: SQLHSTMT) -> SQLRETURN ---
	SQLCloseCursor :: proc(StatementHandle: SQLHSTMT) -> SQLRETURN ---
	SQLColAttribute :: proc(
		StatementHandle: SQLHSTMT,
		ColumnNumber: SQLUSMALLINT,
		FieldIdentifier: SQLUSMALLINT,
		CharacterAttribute: SQLPOINTER,
		BufferLength: SQLSMALLINT,
		StringLength: [^]SQLSMALLINT,
		NumericAttribute: [^]SQLLEN
	) -> SQLRETURN ---
	SQLColumns :: proc(
		StatementHandle: SQLHSTMT,
		CatalogName: [^]SQLCHAR,
		NameLength1: SQLSMALLINT,
		SchemaName: [^]SQLCHAR,
		NameLength2: SQLSMALLINT,
		TableName: [^]SQLCHAR,
		NameLength3: SQLSMALLINT,
		ColumnName: [^]SQLCHAR,
		NameLength4: SQLSMALLINT
	) -> SQLRETURN ---
	SQLConnect :: proc(
		ConnectionHandle: SQLHDBC,
		ServerName: [^]SQLCHAR,
		NameLength1: SQLSMALLINT,
		UserName: [^]SQLCHAR,
		NameLength2: SQLSMALLINT,
		Authentication: [^]SQLCHAR,
		NameLength3: SQLSMALLINT
	) -> SQLRETURN ---
	SQLCopyDesc :: proc(
		SourceDescHandle: SQLHDESC,
		TargetDescHandle: SQLHDESC
	) -> SQLRETURN ---
	SQLDataSources :: proc(
		EnvironmentHandle: SQLHENV,
		Direction: SQLUSMALLINT,
		ServerName: [^]SQLCHAR,
		BufferLength1: SQLSMALLINT,
		NameLength1: [^]SQLSMALLINT,
		Description: [^]SQLCHAR,
		BufferLength2: SQLSMALLINT,
		NameLength2: [^]SQLSMALLINT
	) -> SQLRETURN ---
	SQLDescribeCol :: proc(
		StatementHandle: SQLHSTMT,
		ColumnNumber: SQLUSMALLINT,
		ColumnName: [^]SQLCHAR,
		BufferLength: SQLSMALLINT,
		NameLength: [^]SQLSMALLINT,
		DataType: [^]SQLSMALLINT,
		ColumnSize: [^]SQLULEN,
		DecimalDigits: [^]SQLSMALLINT,
		Nullable: [^]SQLSMALLINT
	) -> SQLRETURN ---
	SQLDisconnect :: proc(ConnectionHandle: SQLHDBC) -> SQLRETURN ---
	SQLEndTran :: proc(
		HandleType: SQLSMALLINT,
		Handle: SQLHANDLE,
		CompletionType: SQLSMALLINT
	) -> SQLRETURN ---
	SQLError :: proc(
		EnvironmentHandle: SQLHENV,
		ConnectionHandle: SQLHDBC,
		StatementHandle: SQLHSTMT,
		Sqlstate: [^]SQLCHAR,
		NativeError: [^]SQLINTEGER,
		MessageText: [^]SQLCHAR,
		BufferLength: SQLSMALLINT,
		TextLength: [^]SQLSMALLINT
	) -> SQLRETURN ---
	SQLExecDirect :: proc(
		StatementHandle: SQLHSTMT,
		StatementText: [^]SQLCHAR,
		TextLength: SQLINTEGER
	) -> SQLRETURN ---
	SQLExecute :: proc(StatementHandle: SQLHSTMT) -> SQLRETURN ---
	SQLFetch :: proc(StatementHandle: SQLHSTMT) -> SQLRETURN ---
	SQLFetchScroll :: proc(
		StatementHandle: SQLHSTMT,
		FetchOrientation: SQLSMALLINT,
		FetchOffset: SQLLEN
	) -> SQLRETURN ---
	SQLFreeConnect :: proc(ConnectionHandle: SQLHDBC) -> SQLRETURN ---
	SQLFreeEnv :: proc(EnvironmentHandle: SQLHENV) -> SQLRETURN ---
	SQLFreeStmt :: proc(
		StatementHandle: SQLHSTMT,
		Option: SQLUSMALLINT
	) -> SQLRETURN ---
	SQLGetConnectAttr :: proc(
		ConnectionHandle: SQLHDBC,
		Attribute: SQLINTEGER,
		Value: SQLPOINTER,
		BufferLength: SQLINTEGER,
		StringLength: [^]SQLINTEGER
	) -> SQLRETURN ---
	SQLGetConnectOption :: proc(
		ConnectionHandle: SQLHDBC,
		Option: SQLUSMALLINT,
		Value: SQLPOINTER
	) -> SQLRETURN ---
	SQLGetCursorName :: proc(
		StatementHandle: SQLHSTMT,
		CursorName: [^]SQLCHAR,
		BufferLength: SQLSMALLINT,
		NameLength: [^]SQLSMALLINT
	) -> SQLRETURN ---
	SQLGetData :: proc(
		StatementHandle: SQLHSTMT,
		ColumnNumber: SQLUSMALLINT,
		TargetType: SQLSMALLINT,
		TargetValue: SQLPOINTER,
		BufferLength: SQLLEN,
		StrLen_or_Ind: [^]SQLLEN
	) -> SQLRETURN ---
	SQLGetDescField :: proc(
		DescriptorHandle: SQLHDESC,
		RecNumber: SQLSMALLINT,
		FieldIdentifier: SQLSMALLINT,
		Value: SQLPOINTER,
		BufferLength: SQLINTEGER,
		StringLength: [^]SQLINTEGER
	) -> SQLRETURN ---
	SQLGetDescRec :: proc(
		DescriptorHandle: SQLHDESC,
		RecNumber: SQLSMALLINT,
		Name: [^]SQLCHAR,
		BufferLength: SQLSMALLINT,
		StringLength: [^]SQLSMALLINT,
		Type: [^]SQLSMALLINT,
		SubType: [^]SQLSMALLINT,
		Length: [^]SQLLEN,
		Precision: [^]SQLSMALLINT,
		Scale: [^]SQLSMALLINT,
		Nullable: [^]SQLSMALLINT
	) -> SQLRETURN ---
	SQLGetDiagField :: proc(
		HandleType: SQLSMALLINT,
		Handle: SQLHANDLE,
		RecNumber: SQLSMALLINT,
		DiagIdentifier: SQLSMALLINT,
		DiagInfo: SQLPOINTER,
		BufferLength: SQLSMALLINT,
		StringLength: [^]SQLSMALLINT
	) -> SQLRETURN ---
	SQLGetDiagRec :: proc(
		HandleType: SQLSMALLINT,
		Handle: SQLHANDLE,
		RecNumber: SQLSMALLINT,
		Sqlstate: [^]SQLCHAR,
		NativeError: [^]SQLINTEGER,
		MessageText: [^]SQLCHAR,
		BufferLength: SQLSMALLINT,
		TextLength: [^]SQLSMALLINT
	) -> SQLRETURN ---
	SQLGetEnvAttr :: proc(
		EnvironmentHandle: SQLHENV,
		Attribute: SQLINTEGER,
		Value: SQLPOINTER,
		BufferLength: SQLINTEGER,
		StringLength: [^]SQLINTEGER
	) -> SQLRETURN ---
	SQLGetFunctions :: proc(
		ConnectionHandle: SQLHDBC,
		FunctionId: SQLUSMALLINT,
		Supported: [^]SQLUSMALLINT
	) -> SQLRETURN ---
	SQLGetInfo :: proc(
		ConnectionHandle: SQLHDBC,
		InfoType: SQLUSMALLINT,
		InfoValue: SQLPOINTER,
		BufferLength: SQLSMALLINT,
		StringLength: [^]SQLSMALLINT
	) -> SQLRETURN ---
	SQLGetStmtAttr :: proc(
		StatementHandle: SQLHSTMT,
		Attribute: SQLINTEGER,
		Value: SQLPOINTER,
		BufferLength: SQLINTEGER,
		StringLength: [^]SQLINTEGER
	) -> SQLRETURN ---
	SQLGetStmtOption :: proc(
		StatementHandle: SQLHSTMT,
		Option: SQLUSMALLINT,
		Value: SQLPOINTER
	) -> SQLRETURN ---
	SQLGetTypeInfo :: proc(
		StatementHandle: SQLHSTMT,
		DataType: SQLSMALLINT
	) -> SQLRETURN ---
	SQLNumResultCols :: proc(
		StatementHandle: SQLHSTMT,
		ColumnCount: [^]SQLSMALLINT
	) -> SQLRETURN ---
	SQLParamData :: proc(
		StatementHandle: SQLHSTMT,
		Value: [^]SQLPOINTER
	) -> SQLRETURN ---
	SQLPrepare :: proc(
		StatementHandle: SQLHSTMT,
		StatementText: [^]SQLCHAR,
		TextLength: SQLINTEGER
	) -> SQLRETURN ---
	SQLPutData :: proc(
		StatementHandle: SQLHSTMT,
		Data: SQLPOINTER,
		StrLen_or_Ind: SQLLEN
	) -> SQLRETURN ---
	SQLRowCount :: proc(
		StatementHandle: SQLHSTMT,
		RowCount: [^]SQLLEN
	) -> SQLRETURN ---
	SQLSetConnectAttr :: proc(
		ConnectionHandle: SQLHDBC,
		Attribute: SQLINTEGER,
		Value: SQLPOINTER,
		StringLength: SQLINTEGER
	) -> SQLRETURN ---
	SQLSetConnectOption :: proc(
		ConnectionHandle: SQLHDBC,
		Option: SQLUSMALLINT,
		Value: SQLULEN
	) -> SQLRETURN ---
	SQLSetCursorName :: proc(
		StatementHandle: SQLHSTMT,
		CursorName: [^]SQLCHAR,
		NameLength: SQLSMALLINT
	) -> SQLRETURN ---
	SQLSetDescField :: proc(
		DescriptorHandle: SQLHDESC,
		RecNumber: SQLSMALLINT,
		FieldIdentifier: SQLSMALLINT,
		Value: SQLPOINTER,
		BufferLength: SQLINTEGER
	) -> SQLRETURN ---
	SQLSetDescRec :: proc(
		DescriptorHandle: SQLHDESC,
		RecNumber: SQLSMALLINT,
		Type: SQLSMALLINT,
		SubType: SQLSMALLINT,
		Length: SQLLEN,
		Precision: SQLSMALLINT,
		Scale: SQLSMALLINT,
		Data: SQLPOINTER,
		StringLength: [^]SQLLEN,
		Indicator: [^]SQLLEN
	) -> SQLRETURN ---
	SQLSetEnvAttr :: proc(
		EnvironmentHandle: SQLHENV,
		Attribute: SQLINTEGER,
		Value: SQLPOINTER,
		StringLength: SQLINTEGER
	) -> SQLRETURN ---
	SQLSetParam :: proc(
		StatementHandle: SQLHSTMT,
		ParameterNumber: SQLUSMALLINT,
		ValueType: SQLSMALLINT,
		ParameterType: SQLSMALLINT,
		LengthPrecision: SQLULEN,
		ParameterScale: SQLSMALLINT,
		ParameterValue: SQLPOINTER,
		StrLen_or_Ind: [^]SQLLEN
	) -> SQLRETURN ---
	SQLSetStmtAttr :: proc(
		StatementHandle: SQLHSTMT,
		Attribute: SQLINTEGER,
		Value: SQLPOINTER,
		StringLength: SQLINTEGER
	) -> SQLRETURN ---
	SQLSetStmtOption :: proc(
		StatementHandle: SQLHSTMT,
		Option: SQLUSMALLINT,
		Value: SQLULEN
	) -> SQLRETURN ---
	SQLSpecialColumns :: proc(
		StatementHandle: SQLHSTMT,
		IdentifierType: SQLUSMALLINT,
		CatalogName: [^]SQLCHAR,
		NameLength1: SQLSMALLINT,
		SchemaName: [^]SQLCHAR,
		NameLength2: SQLSMALLINT,
		TableName: [^]SQLCHAR,
		NameLength3: SQLSMALLINT,
		Scope: SQLUSMALLINT,
		Nullable: SQLUSMALLINT
	) -> SQLRETURN ---
	SQLStatistics :: proc(
		StatementHandle: SQLHSTMT,
		CatalogName: [^]SQLCHAR,
		NameLength1: SQLSMALLINT,
		SchemaName: [^]SQLCHAR,
		NameLength2: SQLSMALLINT,
		TableName: [^]SQLCHAR,
		NameLength3: SQLSMALLINT,
		Unique: SQLUSMALLINT,
		Reserved: SQLUSMALLINT
	) -> SQLRETURN ---
	SQLTables :: proc(
		StatementHandle: SQLHSTMT,
		CatalogName: [^]SQLCHAR,
		NameLength1: SQLSMALLINT,
		SchemaName: [^]SQLCHAR,
		NameLength2: SQLSMALLINT,
		TableName: [^]SQLCHAR,
		NameLength3: SQLSMALLINT,
		TableType: [^]SQLCHAR,
		NameLength4: SQLSMALLINT
	) -> SQLRETURN ---
	SQLTransact :: proc(
		EnvironmentHandle: SQLHENV,
		ConnectionHandle: SQLHDBC,
		CompletionType: SQLUSMALLINT
	) -> SQLRETURN ---
}

SqlReturn :: enum i16
{
	SUCCESS = 0,
	SUCCESS_WITH_INFO = 1,
	NO_DATA = 100,
	PARAM_DATA_AVAILABLE = 101,
	ERROR = -1,
	INVALID_HANDLE = -2,
	STILL_EXECUTING = 2,
	NEED_DATA = 99,
}

SqlHandleType :: enum i16
{
	ENV = 1,
	DBC = 2,
	STMT = 3,
	DESC = 4,
}

SqlEnvAttr :: enum i32
{
	OUTPUT_NTS = 10001,
}

SqlConnectAttr :: enum i32
{
	AUTO_IPD = 10001,
	METADATA_ID = 10014,
}

SqlStmtAttr :: enum i32
{
	APP_ROW_DESC = 10010,
	APP_PARAM_DESC = 10011,
	IMP_ROW_DESC = 10012,
	IMP_PARAM_DESC = 10013,
	CURSOR_SCROLLABLE = -1,
	CURSOR_SENSITIVITY = -2,
}

SqlCursorScrollable :: enum int
{
	NONSCROLLABLE = 0,
	SCROLLABLE = 1,
}

SqlDbType :: enum i16
{
	UNKNOWN_TYPE = 0,
	CHAR = 1,
	NUMERIC = 2,
	DECIMAL = 3,
	INTEGER = 4,
	SMALLINT = 5,
	FLOAT = 6,
	REAL = 7,
	DOUBLE = 8,
	DATETIME = 9,
	VARCHAR = 12,
}

SqlFunctionIdentifier :: enum u16
{
	ALLOCCONNECT = 1,
	ALLOCENV = 2,
	ALLOCHANDLE = 1001,
	ALLOCSTMT = 3,
	BINDCOL = 4,
	BINDPARAM = 1002,
	CANCEL = 5,
	CLOSECURSOR = 1003,
	COLATTRIBUTE = 6,
	COLUMNS = 40,
	CONNECT = 7,
	COPYDESC = 1004,
	DATASOURCES = 57,
	DESCRIBECOL = 8,
	DISCONNECT = 9,
	ENDTRAN = 1005,
	ERROR = 10,
	EXECDIRECT = 11,
	EXECUTE = 12,
	FETCH = 13,
	FETCHSCROLL = 1021,
	FREECONNECT = 14,
	FREEENV = 15,
	FREEHANDLE = 1006,
	FREESTMT = 16,
	GETCONNECTATTR = 1007,
	GETCONNECTOPTION = 42,
	GETCURSORNAME = 17,
	GETDATA = 43,
	GETDESCFIELD = 1008,
	GETDESCREC = 1009,
	GETDIAGFIELD = 1010,
	GETDIAGREC = 1011,
	GETENVATTR = 1012,
	GETFUNCTIONS = 44,
	GETINFO = 45,
	GETSTMTATTR = 1014,
	GETSTMTOPTION = 46,
	GETTYPEINFO = 47,
	NUMRESULTCOLS = 18,
	PARAMDATA = 48,
	PREPARE = 19,
	PUTDATA = 49,
	ROWCOUNT = 20,
	SETCONNECTATTR = 1016,
	SETCONNECTOPTION = 50,
	SETCURSORNAME = 21,
	SETDESCFIELD = 1017,
	SETDESCREC = 1018,
	SETENVATTR = 1019,
	SETPARAM = 22,
	SETSTMTATTR = 1020,
	SETSTMTOPTION = 51,
	SPECIALCOLUMNS = 52,
	STATISTICS = 53,
	TABLES = 54,
	TRANSACT = 23,
	CANCELHANDLE = 1022,
}

SqlTransactionCompletionOptions :: enum i16
{
	COMMIT = 0,
	ROLLBACK = 1,
}

DataSourceInfo :: struct
{
	serverName: string,
	description: string,
}

ColumnInfo :: struct
{
	columnName: string,
	columnSize: u32,
	dbType: SqlDbType,
	decimalDigits: i16,
	nillable: b16,
}

SqlReturnDataSources :: union
{
	SqlReturn,
	DataSourceInfo
}

SqlReturnColumnInfo :: union
{
	SqlReturn,
	ColumnInfo
}



AllocConnect :: proc(
	EnvironmentHandle: SQLHENV,
	ConnectionHandle: [^]SQLHDBC
) -> SqlReturn
{
	return transmute(SqlReturn)SQLAllocConnect(
		EnvironmentHandle,
		ConnectionHandle
	);
}

AllocEnv :: proc(EnvironmentHandle: [^]SQLHENV) -> SqlReturn
{
	return transmute(SqlReturn)SQLAllocEnv(EnvironmentHandle);
}

AllocHandle :: proc { AllocConnect, AllocEnv, AllocStmt, }

AllocStmt :: proc(
	ConnectionHandle: SQLHDBC,
	StatementHandle: [^]SQLHSTMT
) -> SqlReturn
{
	return transmute(SqlReturn)SQLAllocStmt(ConnectionHandle, StatementHandle);
}

BindCol :: proc(
	StatementHandle: SQLHSTMT,
	ColumnNumber: SQLUSMALLINT,
	TargetType: SQLSMALLINT,
	TargetValue: SQLPOINTER,
	BufferLength: SQLLEN,
	StrLen_or_Ind: [^]SQLLEN
) -> SqlReturn
{
	return transmute(SqlReturn)SQLBindCol(
		StatementHandle,
		ColumnNumber,
		TargetType,
		TargetValue,
		BufferLength,
		StrLen_or_Ind
	);
}

BindParam :: proc(
	StatementHandle: SQLHSTMT,
	ParameterNumber: SQLUSMALLINT,
	ValueType: SQLSMALLINT,
	ParameterType: SQLSMALLINT,
	LengthPrecision: SQLULEN,
	ParameterScale: SQLSMALLINT,
	ParameterValue: SQLPOINTER,
	StrLen_or_Ind: ^SQLLEN
) -> SqlReturn
{
	return transmute(SqlReturn)SQLBindParam(
		StatementHandle,
		ParameterNumber,
		ValueType,
		ParameterType,
		LengthPrecision,
		ParameterScale,
		ParameterValue,
		StrLen_or_Ind
	);
}

Cancel :: proc(StatementHandle: SQLHSTMT) -> SqlReturn
{
	return transmute(SqlReturn)SQLCancel(StatementHandle);
}

CancelHandle :: proc { Cancel, }

CloseCursor :: proc(StatementHandle: SQLHSTMT) -> SqlReturn
{
	return transmute(SqlReturn)SQLCloseCursor(StatementHandle);
}

ColAttribute :: proc(
	StatementHandle: SQLHSTMT,
	ColumnNumber: SQLUSMALLINT,
	FieldIdentifier: SQLUSMALLINT,
	CharacterAttribute: SQLPOINTER,
	BufferLength: SQLSMALLINT,
	StringLength: ^SQLSMALLINT,
	NumericAttribute: ^SQLLEN
) -> SqlReturn
{
	return transmute(SqlReturn)SQLColAttribute(
		StatementHandle,
		ColumnNumber,
		FieldIdentifier,
		CharacterAttribute,
		BufferLength,
		StringLength,
		NumericAttribute
	);
}

Columns :: proc(
	StatementHandle: SQLHSTMT,
	CatalogName: string,
	SchemaName: string,
	TableName: string,
	ColumnName: string,
) -> SqlReturn
{
	return transmute(SqlReturn)SQLColumns(
		StatementHandle,
		raw_data(CatalogName),
		cast(i16)len(CatalogName),
		raw_data(SchemaName),
		cast(i16)len(SchemaName),
		raw_data(TableName),
		cast(i16)len(TableName),
		raw_data(ColumnName),
		cast(i16)len(ColumnName)
	);
}

Connect :: proc(
	ConnectionHandle: SQLHDBC,
	ServerName: string,
	UserName: string,
	Authentication: string,
) -> SqlReturn
{
	return transmute(SqlReturn)SQLConnect(
		ConnectionHandle,
		raw_data(ServerName),
		cast(i16)len(ServerName),
		raw_data(UserName),
		cast(i16)len(UserName),
		raw_data(Authentication),
		cast(i16)len(Authentication)
	);
}

CopyDesc :: proc(
	SourceDescHandle: SQLHDESC,
	TargetDescHandle: SQLHDESC
) -> SqlReturn
{
	return transmute(SqlReturn)SQLCopyDesc(SourceDescHandle, TargetDescHandle);
}

DataSources :: proc(
	EnvironmentHandle: SQLHENV,
	Direction: SQLUSMALLINT,
	allocator: mem.Allocator = context.allocator
) -> (ret: SqlReturnDataSources, err: mem.Allocator_Error)
{
	serverNameLength: i16;
	descriptionLength: i16;
	buffer: [^]u8 = make([^]u8, 2 * 0x7FFF, allocator) or_return;
	cRet: SqlReturn = transmute(SqlReturn)SQLDataSources(
		EnvironmentHandle,
		Direction,
		buffer,
		0x7FFF,
		&serverNameLength,
		&(buffer[0x7FFF]),
		0x7FFF,
		&descriptionLength
	);
	info: DataSourceInfo;
	#partial switch cRet
	{
	case .SUCCESS, .SUCCESS_WITH_INFO, .PARAM_DATA_AVAILABLE:
		if serverNameLength != 0
		{
			info.serverName = strings.string_from_ptr(
				buffer,
				cast(int)serverNameLength
			);
		}
		if descriptionLength != 0
		{
			info.description = strings.string_from_ptr(
				&(buffer[0x7FFF]),
				cast(int)descriptionLength
			);
		}
	case:
		return cRet, .None;
	}
	return info, .None;
}

DescribeCol :: proc(
	StatementHandle: SQLHSTMT,
	ColumnNumber: SQLUSMALLINT,
	allocator: mem.Allocator = context.allocator
) -> (ret: SqlReturnColumnInfo, err: mem.Allocator_Error)
{
	columnNameLength: i16;
	dataType: i16;
	columnSize: u32;
	decimalDigits: i16;
	nullable: b16;
	buffer: [^]u8 = make([^]u8, 0x7FFF, allocator) or_return;
	cRet: SqlReturn = transmute(SqlReturn)SQLDescribeCol(
		StatementHandle,
		ColumnNumber,
		buffer,
		0x7FFF,
		&columnNameLength,
		&dataType,
		&columnSize,
		&decimalDigits,
		transmute(^i16)(&nullable)
	);
	info: ColumnInfo;
	#partial switch cRet
	{
	case .SUCCESS, .SUCCESS_WITH_INFO, .PARAM_DATA_AVAILABLE:
		info = {
			columnSize = columnSize,
			dbType = transmute(SqlDbType)dataType,
			decimalDigits = decimalDigits,
			nillable = nullable,
		};
		if columnNameLength != 0
		{
			info.columnName = strings.string_from_ptr(
				buffer,
				cast(int)columnNameLength
			);
		}
	case:
		return cRet, .None;
	}
	return info, .None;
}

Disconnect :: proc(ConnectionHandle: SQLHDBC) -> SqlReturn
{
	return transmute(SqlReturn)SQLDisconnect(ConnectionHandle);
}

EndTran :: proc(
	HandleType: SqlHandleType,
	Handle: SQLHANDLE,
	CompletionType: SqlTransactionCompletionOptions
) -> SqlReturn
{
	return transmute(SqlReturn)SQLEndTran(
		transmute(i16)HandleType,
		Handle,
		transmute(i16)CompletionType
	);
}

Error :: proc(
	EnvironmentHandle: SQLHENV,
	ConnectionHandle: SQLHDBC,
	StatementHandle: SQLHSTMT,
	Sqlstate: ^SQLCHAR,
	NativeError: ^SQLINTEGER,
	MessageText: ^SQLCHAR,
	BufferLength: SQLSMALLINT,
	TextLength: ^SQLSMALLINT
) -> SqlReturn
{
	return transmute(SqlReturn)SQLError(
		EnvironmentHandle,
		ConnectionHandle,
		StatementHandle,
		Sqlstate,
		NativeError,
		MessageText,
		BufferLength,
		TextLength
	);
}

ExecDirect :: proc(
	StatementHandle: SQLHSTMT,
	StatementText: string
) -> SqlReturn
{
	return transmute(SqlReturn)SQLExecDirect(
		StatementHandle,
		raw_data(StatementText),
		cast(i32)len(StatementText)
	);
}

Execute :: proc(StatementHandle: SQLHSTMT) -> SqlReturn
{
	return transmute(SqlReturn)SQLExecute(StatementHandle);
}

Fetch :: proc(StatementHandle: SQLHSTMT) -> SqlReturn
{
	return transmute(SqlReturn)SQLFetch(StatementHandle);
}

FetchScroll :: proc(
	StatementHandle: SQLHSTMT,
	FetchOrientation: SQLSMALLINT,
	FetchOffset: SQLLEN
) -> SqlReturn
{
	return transmute(SqlReturn)SQLFetchScroll(
		StatementHandle,
		FetchOrientation,
		FetchOffset
	);
}

FreeConnect :: proc(ConnectionHandle: SQLHDBC) -> SqlReturn
{
	return transmute(SqlReturn)SQLFreeConnect(ConnectionHandle);
}

FreeEnv :: proc(EnvironmentHandle: SQLHENV) -> SqlReturn
{
	return transmute(SqlReturn)SQLFreeEnv(EnvironmentHandle);
}

FreeHandle :: proc { FreeConnect, FreeEnv, FreeStmt, }

FreeStmt :: proc(StatementHandle: SQLHSTMT, Option: SQLUSMALLINT) -> SqlReturn
{
	return transmute(SqlReturn)SQLFreeStmt(StatementHandle, Option);
}

GetConnectAttr :: proc(
	ConnectionHandle: SQLHDBC,
	Attribute: SqlConnectAttr,
	Value: SQLPOINTER,
	BufferLength: SQLINTEGER,
	StringLength: ^SQLINTEGER
) -> SqlReturn
{
	return transmute(SqlReturn)SQLGetConnectAttr(
		ConnectionHandle,
		transmute(i32)Attribute,
		Value,
		BufferLength,
		StringLength
	);
}

GetConnectOption :: proc(
	ConnectionHandle: SQLHDBC,
	Option: SQLUSMALLINT,
	Value: SQLPOINTER
) -> SqlReturn
{
	return transmute(SqlReturn)SQLGetConnectOption(
		ConnectionHandle,
		Option,
		Value
	);
}

GetCursorName :: proc(
	StatementHandle: SQLHSTMT,
	CursorName: ^SQLCHAR,
	BufferLength: SQLSMALLINT,
	NameLength: ^SQLSMALLINT
) -> SqlReturn
{
	return transmute(SqlReturn)SQLGetCursorName(
		StatementHandle,
		CursorName,
		BufferLength,
		NameLength
	);
}

GetData :: proc(
	StatementHandle: SQLHSTMT,
	ColumnNumber: SQLUSMALLINT,
	TargetType: SQLSMALLINT,
	TargetValue: SQLPOINTER,
	BufferLength: SQLLEN,
	StrLen_or_Ind: ^SQLLEN
) -> SqlReturn
{
	return transmute(SqlReturn)SQLGetData(
		StatementHandle,
		ColumnNumber,
		TargetType,
		TargetValue,
		BufferLength,
		StrLen_or_Ind
	);
}

GetDescField :: proc(
	DescriptorHandle: SQLHDESC,
	RecNumber: SQLSMALLINT,
	FieldIdentifier: SQLSMALLINT,
	Value: SQLPOINTER,
	BufferLength: SQLINTEGER,
	StringLength: ^SQLINTEGER
) -> SqlReturn
{
	return transmute(SqlReturn)SQLGetDescField(
		DescriptorHandle,
		RecNumber,
		FieldIdentifier,
		Value,
		BufferLength,
		StringLength
	);
}

GetDescRec :: proc(
	DescriptorHandle: SQLHDESC,
	RecNumber: SQLSMALLINT,
	Name: ^SQLCHAR,
	BufferLength: SQLSMALLINT,
	StringLength: ^SQLSMALLINT,
	Type: ^SQLSMALLINT,
	SubType: ^SQLSMALLINT,
	Length: ^SQLLEN,
	Precision: ^SQLSMALLINT,
	Scale: ^SQLSMALLINT,
	Nullable: ^SQLSMALLINT
) -> SqlReturn
{
	return transmute(SqlReturn)SQLGetDescRec(
		DescriptorHandle,
		RecNumber,
		Name,
		BufferLength,
		StringLength,
		Type,
		SubType,
		Length,
		Precision,
		Scale,
		Nullable
	);
}

GetDiagField :: proc(
	HandleType: SqlHandleType,
	Handle: SQLHANDLE,
	RecNumber: SQLSMALLINT,
	DiagIdentifier: SQLSMALLINT,
	DiagInfo: SQLPOINTER,
	BufferLength: SQLSMALLINT,
	StringLength: ^SQLSMALLINT
) -> SqlReturn
{
	return transmute(SqlReturn)SQLGetDiagField(
		transmute(i16)HandleType,
		Handle,
		RecNumber,
		DiagIdentifier,
		DiagInfo,
		BufferLength,
		StringLength
	);
}

GetDiagRec :: proc(
	HandleType: SqlHandleType,
	Handle: SQLHANDLE,
	RecNumber: SQLSMALLINT,
	Sqlstate: ^SQLCHAR,
	NativeError: ^SQLINTEGER,
	MessageText: ^SQLCHAR,
	BufferLength: SQLSMALLINT,
	TextLength: ^SQLSMALLINT
) -> SqlReturn
{
	return transmute(SqlReturn)SQLGetDiagRec(
		transmute(i16)HandleType,
		Handle,
		RecNumber,
		Sqlstate,
		NativeError,
		MessageText,
		BufferLength,
		TextLength
	);
}

GetEnvAttr :: proc(
	EnvironmentHandle: SQLHENV,
	Attribute: SqlEnvAttr,
	Value: SQLPOINTER,
	BufferLength: SQLINTEGER,
	StringLength: ^SQLINTEGER
) -> SqlReturn
{
	return transmute(SqlReturn)SQLGetEnvAttr(
		EnvironmentHandle,
		transmute(i32)Attribute,
		Value,
		BufferLength,
		StringLength
	);
}

GetFunctions :: proc(
	ConnectionHandle: SQLHDBC,
	FunctionId: SqlFunctionIdentifier,
	Supported: ^b16
) -> SqlReturn
{
	return transmute(SqlReturn)SQLGetFunctions(
		ConnectionHandle,
		transmute(u16)FunctionId,
		transmute(^u16)Supported
	);
}

GetInfo :: proc(
	ConnectionHandle: SQLHDBC,
	InfoType: SQLUSMALLINT,
	InfoValue: SQLPOINTER,
	BufferLength: SQLSMALLINT,
	StringLength: ^SQLSMALLINT
) -> SqlReturn
{
	return transmute(SqlReturn)SQLGetInfo(
		ConnectionHandle,
		InfoType,
		InfoValue,
		BufferLength,
		StringLength
	);
}

GetStmtAttr :: proc(
	StatementHandle: SQLHSTMT,
	Attribute: SqlStmtAttr,
	Value: SQLPOINTER,
	BufferLength: SQLINTEGER,
	StringLength: ^SQLINTEGER
) -> SqlReturn
{
	return transmute(SqlReturn)SQLGetStmtAttr(
		StatementHandle,
		transmute(i32)Attribute,
		Value,
		BufferLength,
		StringLength
	);
}

GetStmtOption :: proc(
	StatementHandle: SQLHSTMT,
	Option: SQLUSMALLINT,
	Value: SQLPOINTER
) -> SqlReturn
{
	return transmute(SqlReturn)SQLGetStmtOption(StatementHandle, Option, Value);
}

GetTypeInfo :: proc(
	StatementHandle: SQLHSTMT,
	DataType: SQLSMALLINT
) -> SqlReturn
{
	return transmute(SqlReturn)SQLGetTypeInfo(StatementHandle, DataType);
}

NumResultCols :: proc(
	StatementHandle: SQLHSTMT,
	ColumnCount: ^SQLSMALLINT
) -> SqlReturn
{
	return transmute(SqlReturn)SQLNumResultCols(StatementHandle, ColumnCount);
}

ParamData :: proc(
	StatementHandle: SQLHSTMT,
	Value: ^SQLPOINTER
) -> SqlReturn
{
	return transmute(SqlReturn)SQLParamData(StatementHandle, Value);
}

Prepare :: proc(
	StatementHandle: SQLHSTMT,
	StatementText: string,
) -> SqlReturn
{
	return transmute(SqlReturn)SQLPrepare(
		StatementHandle,
		raw_data(StatementText),
		cast(i32)len(StatementText)
	);
}

PutData :: proc(
	StatementHandle: SQLHSTMT,
	Data: SQLPOINTER,
	StrLen_or_Ind: SQLLEN
) -> SqlReturn
{
	return transmute(SqlReturn)SQLPutData(
		StatementHandle,
		Data,
		StrLen_or_Ind
	);
}

RowCount :: proc(StatementHandle: SQLHSTMT, RowCount: ^SQLLEN) -> SqlReturn
{
	return transmute(SqlReturn)SQLRowCount(StatementHandle, RowCount);
}

SetConnectAttr :: proc(
	ConnectionHandle: SQLHDBC,
	Attribute: SqlConnectAttr,
	Value: SQLPOINTER,
	StringLength: SQLINTEGER
) -> SqlReturn
{
	return transmute(SqlReturn)SQLSetConnectAttr(
		ConnectionHandle,
		transmute(i32)Attribute,
		Value,
		StringLength
	);
}

SetConnectOption :: proc(
	ConnectionHandle: SQLHDBC,
	Option: SQLUSMALLINT,
	Value: SQLULEN
) -> SqlReturn
{
	return transmute(SqlReturn)SQLSetConnectOption(
		ConnectionHandle,
		Option,
		Value
	);
}

SetCursorName :: proc(
	StatementHandle: SQLHSTMT,
	CursorName: string
) -> SqlReturn
{
	return transmute(SqlReturn)SQLSetCursorName(
		StatementHandle,
		raw_data(CursorName),
		cast(i16)len(CursorName)
	);
}

SetDescField :: proc(
	DescriptorHandle: SQLHDESC,
	RecNumber: SQLSMALLINT,
	FieldIdentifier: SQLSMALLINT,
	Value: SQLPOINTER,
	BufferLength: SQLINTEGER
) -> SqlReturn
{
	return transmute(SqlReturn)SQLSetDescField(
		DescriptorHandle,
		RecNumber,
		FieldIdentifier,
		Value,
		BufferLength
	);
}

SetDescRec :: proc(
	DescriptorHandle: SQLHDESC,
	RecNumber: SQLSMALLINT,
	Type: SQLSMALLINT,
	SubType: SQLSMALLINT,
	Length: SQLLEN,
	Precision: SQLSMALLINT,
	Scale: SQLSMALLINT,
	Data: SQLPOINTER,
	StringLength: ^SQLLEN,
	Indicator: ^SQLLEN
) -> SqlReturn
{
	return transmute(SqlReturn)SQLSetDescRec(
		DescriptorHandle,
		RecNumber,
		Type,
		SubType,
		Length,
		Precision,
		Scale,
		Data,
		StringLength,
		Indicator
	);
}

SetEnvAttr :: proc(
	EnvironmentHandle: SQLHENV,
	Attribute: SqlEnvAttr,
	Value: SQLPOINTER,
	StringLength: SQLINTEGER
) -> SqlReturn
{
	return transmute(SqlReturn)SQLSetEnvAttr(
		EnvironmentHandle,
		transmute(i32)Attribute,
		Value,
		StringLength
	);
}

SetParam :: proc(
	StatementHandle: SQLHSTMT,
	ParameterNumber: SQLUSMALLINT,
	ValueType: SQLSMALLINT,
	ParameterType: SQLSMALLINT,
	LengthPrecision: SQLULEN,
	ParameterScale: SQLSMALLINT,
	ParameterValue: SQLPOINTER,
	StrLen_or_Ind: ^SQLLEN
) -> SqlReturn
{
	return transmute(SqlReturn)SQLSetParam(
		StatementHandle,
		ParameterNumber,
		ValueType,
		ParameterType,
		LengthPrecision,
		ParameterScale,
		ParameterValue,
		StrLen_or_Ind
	);
}

SetStmtAttr :: proc(
	StatementHandle: SQLHSTMT,
	Attribute: SqlStmtAttr,
	Value: SQLPOINTER,
	StringLength: SQLINTEGER
) -> SqlReturn
{
	return transmute(SqlReturn)SQLSetStmtAttr(
		StatementHandle,
		transmute(i32)Attribute,
		Value,
		StringLength
	);
}

SetStmtOption :: proc(
	StatementHandle: SQLHSTMT,
	Option: SQLUSMALLINT,
	Value: SQLULEN
) -> SqlReturn
{
	return transmute(SqlReturn)SQLSetStmtOption(
		StatementHandle,
		Option,
		Value
	);
}

SpecialColumns :: proc(
	StatementHandle: SQLHSTMT,
	IdentifierType: SQLUSMALLINT,
	CatalogName: string,
	SchemaName: string,
	TableName: string,
	Scope: SQLUSMALLINT,
	Nullable: SQLUSMALLINT
) -> SqlReturn
{
	return transmute(SqlReturn)SQLSpecialColumns(
		StatementHandle,
		IdentifierType,
		raw_data(CatalogName),
		cast(i16)len(CatalogName),
		raw_data(SchemaName),
		cast(i16)len(SchemaName),
		raw_data(TableName),
		cast(i16)len(TableName),
		Scope,
		Nullable
	);
}

Statistics :: proc(
	StatementHandle: SQLHSTMT,
	CatalogName: string,
	SchemaName: string,
	TableName: string,
	Unique: SQLUSMALLINT,
	Reserved: SQLUSMALLINT
) -> SqlReturn
{
	return transmute(SqlReturn)SQLStatistics(
		StatementHandle,
		raw_data(CatalogName),
		cast(i16)len(CatalogName),
		raw_data(SchemaName),
		cast(i16)len(SchemaName),
		raw_data(TableName),
		cast(i16)len(TableName),
		Unique,
		Reserved
	);
}

Tables :: proc(
	StatementHandle: SQLHSTMT,
	CatalogName: string,
	SchemaName: string,
	TableName: string,
	TableType: string,
) -> SqlReturn
{
	return transmute(SqlReturn)SQLTables(
		StatementHandle,
		raw_data(CatalogName),
		cast(i16)len(CatalogName),
		raw_data(SchemaName),
		cast(i16)len(SchemaName),
		raw_data(TableName),
		cast(i16)len(TableName),
		raw_data(TableType),
		cast(i16)len(TableType)
	);
}

Transact :: proc(
	EnvironmentHandle: SQLHENV,
	ConnectionHandle: SQLHDBC,
	CompletionType: SqlTransactionCompletionOptions
) -> SqlReturn
{
	return transmute(SqlReturn)SQLTransact(
		EnvironmentHandle,
		ConnectionHandle,
		transmute(u16)CompletionType
	);
}



/****************************
 * use these to indicate string termination to some function
 ***************************/
SQL_NTS :: -3;
SQL_NTSL : i64 : -3;

/* maximum message length */
SQL_MAX_MESSAGE_LENGTH :: 512;

/* date/time length constants */
SQL_DATE_LEN :: 10;
SQL_TIME_LEN :: 8;  /* add P+1 if precision is nonzero */
SQL_TIMESTAMP_LEN :: 19;  /* add P+1 if precision is nonzero */

/* identifiers of fields in the SQL descriptor */
SQL_DESC_COUNT :: 1001;
SQL_DESC_TYPE :: 1002;
SQL_DESC_LENGTH :: 1003;
SQL_DESC_OCTET_LENGTH_PTR :: 1004;
SQL_DESC_PRECISION :: 1005;
SQL_DESC_SCALE :: 1006;
SQL_DESC_DATETIME_INTERVAL_CODE :: 1007;
SQL_DESC_NULLABLE :: 1008;
SQL_DESC_INDICATOR_PTR :: 1009;
SQL_DESC_DATA_PTR :: 1010;
SQL_DESC_NAME :: 1011;
SQL_DESC_UNNAMED :: 1012;
SQL_DESC_OCTET_LENGTH :: 1013;
SQL_DESC_ALLOC_TYPE :: 1099;

/* identifiers of fields in the diagnostics area */
SQL_DIAG_RETURNCODE :: 1;
SQL_DIAG_NUMBER :: 2;
SQL_DIAG_ROW_COUNT :: 3;
SQL_DIAG_SQLSTATE :: 4;
SQL_DIAG_NATIVE :: 5;
SQL_DIAG_MESSAGE_TEXT :: 6;
SQL_DIAG_DYNAMIC_FUNCTION :: 7;
SQL_DIAG_CLASS_ORIGIN :: 8;
SQL_DIAG_SUBCLASS_ORIGIN :: 9;
SQL_DIAG_CONNECTION_NAME :: 10;
SQL_DIAG_SERVER_NAME :: 11;
SQL_DIAG_DYNAMIC_FUNCTION_CODE :: 12;

/* dynamic function codes */
SQL_DIAG_ALTER_DOMAIN :: 3;
SQL_DIAG_ALTER_TABLE :: 4;
SQL_DIAG_CALL :: 7;
SQL_DIAG_CREATE_ASSERTION :: 6;
SQL_DIAG_CREATE_CHARACTER_SET :: 8;
SQL_DIAG_CREATE_COLLATION :: 10;
SQL_DIAG_CREATE_DOMAIN :: 23;
SQL_DIAG_CREATE_INDEX :: (-1);
SQL_DIAG_CREATE_SCHEMA :: 64;
SQL_DIAG_CREATE_TABLE :: 77;
SQL_DIAG_CREATE_TRANSLATION :: 79;
SQL_DIAG_CREATE_VIEW :: 84;
SQL_DIAG_DELETE_WHERE :: 19;
SQL_DIAG_DROP_ASSERTION :: 24;
SQL_DIAG_DROP_CHARACTER_SET :: 25;
SQL_DIAG_DROP_COLLATION :: 26;
SQL_DIAG_DROP_DOMAIN :: 27;
SQL_DIAG_DROP_INDEX :: (-2);
SQL_DIAG_DROP_SCHEMA :: 31;
SQL_DIAG_DROP_TABLE :: 32;
SQL_DIAG_DROP_TRANSLATION :: 33;
SQL_DIAG_DROP_VIEW :: 36;
SQL_DIAG_DYNAMIC_DELETE_CURSOR :: 38;
SQL_DIAG_DYNAMIC_UPDATE_CURSOR :: 81;
SQL_DIAG_GRANT :: 48;
SQL_DIAG_INSERT :: 50;
SQL_DIAG_REVOKE :: 59;
SQL_DIAG_SELECT_CURSOR :: 85;
SQL_DIAG_UNKNOWN_STATEMENT :: 0;
SQL_DIAG_UPDATE_WHERE :: 82;

/* One-parameter shortcuts for date/time data types */
SQL_TYPE_DATE :: 91;
SQL_TYPE_TIME :: 92;
SQL_TYPE_TIMESTAMP :: 93;

/* Statement attribute values for cursor sensitivity */
SQL_UNSPECIFIED :: 0;
SQL_INSENSITIVE :: 1;
SQL_SENSITIVE :: 2;

/* GetTypeInfo() request for all data types */
SQL_ALL_TYPES :: 0;

/* Default conversion code for SQLBindCol(), SQLBindParam() and SQLGetData() */
SQL_DEFAULT :: 99;

/* SQLGetData() code indicating that the application row descriptor
 * specifies the data type
 */
SQL_ARD_TYPE :: (-99);

/* SQL date/time type subcodes */
SQL_CODE_DATE :: 1;
SQL_CODE_TIME :: 2;
SQL_CODE_TIMESTAMP :: 3;

/* CLI option values */
SQL_FALSE :: 0;
SQL_TRUE :: 1;

/* Value returned by SQLGetTypeInfo() to denote that it is
 * not known whether or not a data type supports null values.
 */
SQL_NULLABLE_UNKNOWN :: 2;

/* Values returned by SQLGetTypeInfo() to show WHERE clause
 * supported
 */
SQL_PRED_NONE :: 0;
SQL_PRED_CHAR :: 1;
SQL_PRED_BASIC :: 2;

/* values of UNNAMED field in descriptor */
SQL_NAMED :: 0;
SQL_UNNAMED :: 1;

/* values of ALLOC_TYPE field in descriptor */
SQL_DESC_ALLOC_AUTO :: 1;
SQL_DESC_ALLOC_USER :: 2;

/* FreeStmt() options */
SQL_CLOSE :: 0;
SQL_DROP :: 1;
SQL_UNBIND :: 2;
SQL_RESET_PARAMS :: 3;

/* Codes used for FetchOrientation in SQLFetchScroll(),
   and in SQLDataSources()
*/
SQL_FETCH_NEXT :: 1;
SQL_FETCH_FIRST :: 2;

/* Other codes used for FetchOrientation in SQLFetchScroll() */
SQL_FETCH_LAST :: 3;
SQL_FETCH_PRIOR :: 4;
SQL_FETCH_ABSOLUTE :: 5;
SQL_FETCH_RELATIVE :: 6;

/* Values that may appear in the result set of SQLSpecialColumns() */
SQL_SCOPE_CURROW :: 0;
SQL_SCOPE_TRANSACTION :: 1;
SQL_SCOPE_SESSION :: 2;

SQL_PC_UNKNOWN :: 0;
SQL_PC_NON_PSEUDO :: 1;
SQL_PC_PSEUDO :: 2;

/* Reserved value for the IdentifierType argument of SQLSpecialColumns() */
SQL_ROW_IDENTIFIER :: 1;

/* Reserved values for UNIQUE argument of SQLStatistics() */
SQL_INDEX_UNIQUE :: 0;
SQL_INDEX_ALL :: 1;

/* Values that may appear in the result set of SQLStatistics() */
SQL_INDEX_CLUSTERED :: 1;
SQL_INDEX_HASHED :: 2;
SQL_INDEX_OTHER :: 3;

/* Information requested by SQLGetInfo() */
SQL_MAX_DRIVER_CONNECTIONS :: 0;
SQL_MAX_CONCURRENT_ACTIVITIES :: 1;
SQL_DATA_SOURCE_NAME :: 2;
SQL_FETCH_DIRECTION :: 8;
SQL_SERVER_NAME :: 13;
SQL_SEARCH_PATTERN_ESCAPE :: 14;
SQL_DBMS_NAME :: 17;
SQL_DBMS_VER :: 18;
SQL_ACCESSIBLE_TABLES :: 19;
SQL_ACCESSIBLE_PROCEDURES :: 20;
SQL_CURSOR_COMMIT_BEHAVIOR :: 23;
SQL_DATA_SOURCE_READ_ONLY :: 25;
SQL_DEFAULT_TXN_ISOLATION :: 26;
SQL_IDENTIFIER_CASE :: 28;
SQL_IDENTIFIER_QUOTE_CHAR :: 29;
SQL_MAX_COLUMN_NAME_LEN :: 30;
SQL_MAX_CURSOR_NAME_LEN :: 31;
SQL_MAX_SCHEMA_NAME_LEN :: 32;
SQL_MAX_CATALOG_NAME_LEN :: 34;
SQL_MAX_TABLE_NAME_LEN :: 35;
SQL_SCROLL_CONCURRENCY :: 43;
SQL_TXN_CAPABLE :: 46;
SQL_USER_NAME :: 47;
SQL_TXN_ISOLATION_OPTION :: 72;
SQL_INTEGRITY :: 73;
SQL_GETDATA_EXTENSIONS :: 81;
SQL_NULL_COLLATION :: 85;
SQL_ALTER_TABLE :: 86;
SQL_ORDER_BY_COLUMNS_IN_SELECT :: 90;
SQL_SPECIAL_CHARACTERS :: 94;
SQL_MAX_COLUMNS_IN_GROUP_BY :: 97;
SQL_MAX_COLUMNS_IN_INDEX :: 98;
SQL_MAX_COLUMNS_IN_ORDER_BY :: 99;
SQL_MAX_COLUMNS_IN_SELECT :: 100;
SQL_MAX_COLUMNS_IN_TABLE :: 101;
SQL_MAX_INDEX_SIZE :: 102;
SQL_MAX_ROW_SIZE :: 104;
SQL_MAX_STATEMENT_LEN :: 105;
SQL_MAX_TABLES_IN_SELECT :: 106;
SQL_MAX_USER_NAME_LEN :: 107;
SQL_OJ_CAPABILITIES :: 115;
SQL_XOPEN_CLI_YEAR :: 10000;
SQL_CURSOR_SENSITIVITY :: 10001;
SQL_DESCRIBE_PARAMETER :: 10002;
SQL_CATALOG_NAME :: 10003;
SQL_COLLATION_SEQ :: 10004;
SQL_MAX_IDENTIFIER_LEN :: 10005;

/* ALTER_TABLE bitmasks */
SQL_AT_ADD_COLUMN: i64: 0x00000001;
SQL_AT_DROP_COLUMN: i64: 0x00000002;

SQL_AT_ADD_CONSTRAINT :: 0x00000008;

/* The following bitmasks are ODBC extensions and defined in sqlext.h
*AT_COLUMN_SINGLE :: 0x00000020L	
*AT_ADD_COLUMN_DEFAULT :: 0x00000040L
*AT_ADD_COLUMN_COLLATION :: 0x00000080L
*AT_SET_COLUMN_DEFAULT :: 0x00000100L
*AT_DROP_COLUMN_DEFAULT :: 0x00000200L
*AT_DROP_COLUMN_CASCADE :: 0x00000400L
*AT_DROP_COLUMN_RESTRICT :: 0x00000800L
*AT_ADD_TABLE_CONSTRAINT :: 0x00001000L		
*AT_DROP_TABLE_CONSTRAINT_CASCADE :: 0x00002000L		
*AT_DROP_TABLE_CONSTRAINT_RESTRICT :: 0x00004000L		
*AT_CONSTRAINT_NAME_DEFINITION :: 0x00008000L
*AT_CONSTRAINT_INITIALLY_DEFERRED :: 0x00010000L
*AT_CONSTRAINT_INITIALLY_IMMEDIATE :: 0x00020000L
*AT_CONSTRAINT_DEFERRABLE :: 0x00040000L
*AT_CONSTRAINT_NON_DEFERRABLE :: 0x00080000L
*/


/* ASYNC_MODE values */
SQL_AM_NONE :: 0;
SQL_AM_CONNECTION :: 1;
SQL_AM_STATEMENT :: 2;

/* CURSOR_COMMIT_BEHAVIOR values */
SQL_CB_DELETE :: 0;
SQL_CB_CLOSE :: 1;
SQL_CB_PRESERVE :: 2;

/* FETCH_DIRECTION bitmasks */
SQL_FD_FETCH_NEXT: i64: 0x00000001;
SQL_FD_FETCH_FIRST: i64: 0x00000002;
SQL_FD_FETCH_LAST: i64: 0x00000004;
SQL_FD_FETCH_PRIOR: i64: 0x00000008;
SQL_FD_FETCH_ABSOLUTE: i64: 0x00000010;
SQL_FD_FETCH_RELATIVE: i64: 0x00000020;

/* GETDATA_EXTENSIONS bitmasks */
SQL_GD_ANY_COLUMN: i64: 0x00000001;
SQL_GD_ANY_ORDER: i64: 0x00000002;

/* IDENTIFIER_CASE values */
SQL_IC_UPPER :: 1;
SQL_IC_LOWER :: 2;
SQL_IC_SENSITIVE :: 3;
SQL_IC_MIXED :: 4;

/* OJ_CAPABILITIES bitmasks */
/* NB: this means 'outer join', not what  you may be thinking */


SQL_OJ_LEFT: i64: 0x00000001;
SQL_OJ_RIGHT: i64: 0x00000002;
SQL_OJ_FULL: i64: 0x00000004;
SQL_OJ_NESTED: i64: 0x00000008;
SQL_OJ_NOT_ORDERED: i64: 0x00000010;
SQL_OJ_INNER: i64: 0x00000020;
SQL_OJ_ALL_COMPARISON_OPS: i64: 0x00000040;

/* SCROLL_CONCURRENCY bitmasks */
SQL_SCCO_READ_ONLY: i64: 0x00000001;
SQL_SCCO_LOCK: i64: 0x00000002;
SQL_SCCO_OPT_ROWVER: i64: 0x00000004;
SQL_SCCO_OPT_VALUES: i64: 0x00000008;

/* TXN_CAPABLE values */
SQL_TC_NONE :: 0;
SQL_TC_DML :: 1;
SQL_TC_ALL :: 2;
SQL_TC_DDL_COMMIT :: 3;
SQL_TC_DDL_IGNORE :: 4;

/* TXN_ISOLATION_OPTION bitmasks */
SQL_TXN_READ_UNCOMMITTED: i64: 0x00000001;
SQL_TXN_READ_COMMITTED: i64: 0x00000002;
SQL_TXN_REPEATABLE_READ: i64: 0x00000004;
SQL_TXN_SERIALIZABLE: i64: 0x00000008;

/* NULL_COLLATION values */
SQL_NC_HIGH :: 0;
SQL_NC_LOW :: 1;

// STRUCTS
BOOL :: i32;
SQLLEN :: SQLINTEGER;
SQLULEN :: SQLUINTEGER;
SQLSETPOSIROW :: SQLUSMALLINT;
MAX_NUMERIC_LEN :: 16;
ODBCINT64 :: i64;
UODBCINT64 :: u64;
ODBCINT64_TYPE: cstring : "long";
UODBCINT64_TYPE: cstring : "unsigned long";

DATE :: struct
{
	year: i16,
	month: u16,
	day: u16,
}

TIME :: struct
{
	hour: SQLUSMALLINT,
	minute: SQLUSMALLINT,
	second: SQLUSMALLINT,
}

TIMESTAMP :: struct
{
	year: SQLSMALLINT,
	month: SQLUSMALLINT,
	day: SQLUSMALLINT,
	hour: SQLUSMALLINT,
	minute: SQLUSMALLINT,
	second: SQLUSMALLINT,
	fraction: SQLUINTEGER,
}

INTERVAL_TYPE :: enum
{
	IS_YEAR = 1,
	IS_MONTH = 2,
	IS_DAY = 3,
	IS_HOUR = 4,
	IS_MINUTE = 5,
	IS_SECOND = 6,
	IS_YEAR_TO_MONTH = 7,
	IS_DAY_TO_HOUR = 8,
	IS_DAY_TO_MINUTE = 9,
	IS_DAY_TO_SECOND = 10,
	IS_HOUR_TO_MINUTE = 11,
	IS_HOUR_TO_SECOND = 12,
	IS_MINUTE_TO_SECOND = 13,
}

YEAR_MONTH :: struct
{
	year: u32,
	month: u32,
}

DAY_SECOND :: struct
{
	day: u32,
	hour: u32,
	minute: u32,
	second: u32,
	fraction: u32,
}

IntervalUnion :: struct #raw_union
{
	year_month: YEAR_MONTH,
	day_second: DAY_SECOND,
}

INTERVAL :: struct
{
	type: INTERVAL_TYPE,
	sign: i16,
	interval: IntervalUnion,
}

NUMERIC :: struct
{
	precision: u8,
	scale: i8,
	sign: u8,
	val: [MAX_NUMERIC_LEN]u8,
}

GUID :: struct
{
	data1: u32,
	data2: u16,
	data3: u16,
	data4: [8]u8,
}

HWND :: distinct rawptr;
SQLHWND :: distinct rawptr;
HINSTANCE :: distinct rawptr;
SQLPOINTER :: distinct rawptr;
SQLHANDLE :: distinct rawptr;
SQLHENV :: distinct rawptr;
SQLHDBC :: distinct rawptr;
SQLHSTMT :: distinct rawptr;
PTR :: distinct rawptr;
SQLHDESC :: distinct rawptr;

SQLINTEGER :: i32;
SQLUINTEGER :: u32;
CHAR :: u8;
WCHAR :: u16;
BYTE :: u8;
WORD :: u16;
DWORD :: u32;

LPWSTR :: [^]WCHAR;
LPCSTR :: [^]u8;
LPTSTR :: [^]TCHAR;
LPSTR :: [^]u8;
LPDWORD :: [^]u32;

when ODIN_OS == .Windows
{
	TCHAR :: i16;
}
else
{
	TCHAR :: u8;
	UCHAR :: u8;
	SCHAR :: i8;
	SQLSCHAR :: SCHAR;
	SDWORD :: i32;
	UDWORD :: u32;
	SWORD :: i16;
	UWORD :: u16;
	UINT :: u32;
	SLONG :: i32;
	SSHORT :: i16;
	ULONG :: u32;
	USHORT :: u16;
	SDOUBLE :: f64;
	LDOUBLE :: f64;
	SFLOAT :: f32;
}

SQLCHAR :: u8;
SQLDATE :: u8;
SQLDECIMAL :: u8;
SQLDOUBLE :: f64;
SQLFLOAT :: f64;
SQLREAL :: f32;

SQLROWCOUNT :: SQLULEN;
SQLROWSETSIZE :: SQLULEN;
SQLTRANSID :: SQLULEN;
SQLROWOFFSET :: SQLLEN;

SQLNUMERIC :: u8;
SQLSMALLINT :: i16;
SQLUSMALLINT :: u16;

SQLTIME :: u8;
SQLTIMESTAMP :: u8;
SQLVARCHAR :: u8;
SQLRETURN :: i16;
