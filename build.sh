#!/bin/bash

PROJ_NAME="spamBotRemoval"
ODIN_COMPILER="odin"
OBJ_PATH="build/$1/obj/$PROJ_NAME"
BIN_PATH="build/$1/bin/$PROJ_NAME"
COMMON_ODIN_FLAGS=" \
-out:$OBJ_PATH \
-keep-temp-files \
"
DEBUG_ODIN_FLAGS=" \
-o:none \
-debug \
"
RELEASE_ODIN_FLAGS="\
-o:aggressive \
-disable-assert \
-no-bounds-check \
"
FILES="src"

mkdir -p build/{debug,release}/{obj,bin}

case $1 in
	"debug")
		eval "$ODIN_COMPILER build $FILES $DEBUG_ODIN_FLAGS $COMMON_ODIN_FLAGS"
		mv $OBJ_PATH $BIN_PATH
		;;
	"release")
		eval "$ODIN_COMPILER build $FILES $RELEASE_ODIN_FLAGS $COMMON_ODIN_FLAGS"
		mv $OBJ_PATH $BIN_PATH
		;;
	"clean")
		rm -rf build
		;;
esac
